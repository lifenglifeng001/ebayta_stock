# -*- coding: utf-8 -*-

from openerp import models, fields

class Brand(models.Model):
    _description = 'Brand'
    _name = 'ebayta_stock.brand'

    name = fields.Char('Name')


class BrandCategory(models.Model):
    _description = 'Brand Category'
    _name = 'ebayta_stock.brand.category'

    name = fields.Char('Name')
    factory = fields.Many2one('res.partner', string='Factory')

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    application = fields.Selection(
        [('men_clothes', 'Men clothes'), ('women_clothes', 'Women clothes'), ('children_clothes', 'Children Clothes')],
        'Application people'
    )
    brand = fields.Many2one('ebayta_stock.brand', string="Brand")
    brand_category = fields.Many2one('ebayta_stock.brand.category', string="Brand Category")

class ProductProduct(models.Model):
    _inherit = 'product.product'

    brand = fields.Many2one(related="product_tmpl_id.brand", string="Brand")
    brand_category = fields.Many2one(related='product_tmpl_id.brand_category', string="Brand category")
    application = fields.Selection(related="product_tmpl_id.application", string="Application people")
