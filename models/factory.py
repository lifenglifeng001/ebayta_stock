# -*- coding: utf-8 -*-

from openerp import models, fields, api

class res_partner(models.Model):

    _inherit = "res.partner"

    def _compute_default_factory(self):
        if 'factory' in self._context and self._context['factory']:
            return True

    def _compute_default_customer(self):
        if 'customer' in self._context and self._context['customer']:
            return True

    factory = fields.Boolean("Is a factory", default=_compute_default_factory)
    customer = fields.Boolean("Is a customer", default=_compute_default_customer,
                              help="Check this box if this contact is a customer.")

    @api.onchange('factory', )
    def _onchange_factory(self):
        if self.factory:
            self.supplier = True
