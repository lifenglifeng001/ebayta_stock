# -*- coding: utf-8 -*-

from openerp import models, fields, api
import random

class stock_picking(models.Model):

    _inherit = "stock.picking"

    employee_id = fields.Many2one('hr.employee', 'Employee', states={'done': [('readonly', True)]})

    @api.multi
    def write(self, vals):
        result = super(stock_picking, self).write(vals)
        if vals.get('employee_id', False):
            self.action_assign()
            self.action_done()
        return result

class stock_move(models.Model):

    _inherit = 'stock.move'

    label_code = fields.Char('Lable code', readonly=True)

    def create(self, cr, uid, default, context=None):
        if default.get('label_code', False):
            default.update({'label_code': str(random.randint(10000000, 99999999))})
        return super(stock_move, self).create(cr, uid, default, context=context)


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    @api.model
    def _run_move_create(self, procurement):

        vals = super(ProcurementOrder, self)._run_move_create(procurement)
        if self.sale_line_id:
            vals.update({
                'sequence': self.sale_line_id.sequence,
            })

        if procurement.sale_line_id.order_id.delivery_method != 'expres':
            vals.update({
                'label_code': True,
            })
        return vals
